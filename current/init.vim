" Make the vim clipboard sync with the system clipboard.
set clipboard+=unnamedplus
" This is used so fancy stuff works!
set nocompatible
" Set terminal colour to 256 colours.
set t_Co=256
" Set tabs to 4 chars.
set tabstop=4
" Set the indentation level to 4 chars.
set shiftwidth=4
" Convert tabs to spaces
set expandtab
" Show the current number line and all the lines relative to it.
set number relativenumber
" Automatically indent
set autoindent
" Highlight all search matches
set hlsearch
" Split below and to the right by default
set splitbelow
set splitright
" Show invisible characters
set hidden
" Enable true colours
set termguicolors
set mouse=

" Autostart auto-complete
let g:coq_settings = { 'auto_start': 'shut-up' }

lua require('init')

colorscheme catppuccin-macchiato

" Autocomplete {}
let g:xptemplate_brace_complete = '{'

""" Shortcuts for windows and splits manipulation
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Right> :wincmd l<CR>

nmap <silent> <A-C-Left> :vertical resize +5<CR>
nmap <silent> <A-C-Down> :resize +5<CR>
nmap <silent> <A-C-Up> :resize -5<CR>
nmap <silent> <A-C-Right> :vertical resize -5<CR>

""" Clear search
nmap <silent> <C-/> :noh<CR> 

""" Neotree open and close
map <C-n> :Neotree toggle reveal_force_cwd<CR>

" Map ToggleBool to `Ctrl + q`
noremap <C-q> :ToggleBool<CR>

" Highlight yank duration to 1 second.
let g:highlightedyank_highlight_duration = 1000

" Easy Align
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Highlight time in ms
let g:Illuminate_delay = 250

" Bind F3 to format the code
noremap <F3> :Autoformat<CR>

" plasticboy/vim-markdown
set conceallevel=0
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0

" Extending alignment rules
let g:easy_align_delimiters = {
            \ '>': { 'pattern': '>>\|=>\|>' },
            \ '/': {
            \     'pattern':         '//\+\|/\*\|\*/',
            \     'delimiter_align': 'l',
            \     'ignore_groups':   ['!Comment'] },
            \ ']': {
            \     'pattern':       '[[\]]',
            \     'left_margin':   0,
            \     'right_margin':  0,
            \     'stick_to_left': 0
            \   },
            \ ')': {
            \     'pattern':       '[()]',
            \     'left_margin':   0,
            \     'right_margin':  0,
            \     'stick_to_left': 0
            \   },
            \ 'd': {
            \     'pattern':      ' \(\S\+\s*[;=]\)\@=',
            \     'left_margin':  0,
            \     'right_margin': 0
            \   },
            \ ';': {
            \     'pattern': ';',
            \     'ignore_groups': ['String'],
            \     'ignore_unmatched': 0
            \  }
            \ }

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved
set signcolumn=yes

autocmd CursorHold * lua vim.diagnostic.open_float(nil, { focusable = false })

" Toruble keybinds
nnoremap <leader>xx <cmd>TroubleToggle<cr>
nnoremap <leader>xw <cmd>TroubleToggle workspace_diagnostics<cr>
nnoremap <leader>xd <cmd>TroubleToggle document_diagnostics<cr>
nnoremap <leader>xq <cmd>TroubleToggle quickfix<cr>
nnoremap <leader>xl <cmd>TroubleToggle loclist<cr>
nnoremap gR <cmd>TroubleToggle lsp_references<cr>

" Open Trouble
map <C-t> :TroubleToggle<CR>

" Vimspector debugger
let g:vimspector_sidebar_width = 85
let g:vimspector_bottombar_height = 15
let g:vimspector_terminal_maxwidth = 70

"nmap <F9> <cmd>call vimspector#Launch()<cr>
"nmap <F5> <cmd>call vimspector#StepOver()<cr>
"nmap <F8> <cmd>call vimspector#Reset()<cr>
"nmap <F11> <cmd>call vimspector#StepOver()<cr>")
"nmap <F12> <cmd>call vimspector#StepOut()<cr>")
"nmap <F10> <cmd>call vimspector#StepInto()<cr>")
"lua map('n', "Db", ":call vimspector#ToggleBreakpoint()<cr>")
"lua map('n', "Dw", ":call vimspector#AddWatch()<cr>")
"lua map('n', "De", ":call vimspector#Evaluate()<cr>")

" for normal mode - the word under the cursor
nmap <Leader>di <Plug>VimspectorBalloonEval
" for visual mode, the visually selected text
xmap <Leader>di <Plug>VimspectorBalloonEval

let g:vimspector_enable_mappings = 'VISUAL_STUDIO'

" Disable zig autoformat on save
let g:zig_fmt_autosave = 0

let g:formatdef_ruff = '"ruff format -"'
let g:formatters_python = ['ruff']
let g:run_all_formatters_python = 1

" Auto start Neotree
autocmd BufRead * Neotree show reveal_force_cwd
