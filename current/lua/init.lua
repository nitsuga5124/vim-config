require("plugins")
require("catppuccin").setup()
require("lualine").setup{
    options = {
        theme = "catppuccin"
    },
    sections = {
        lualine_c = {
            "lsp_progress"
        }
    }
}

-- Language servers
require("lsp")

-- rust LSP
local rt = require("rust-tools")

rt.setup({
    server = {
        on_attach = function(_, bufnr)
            -- Hover actions
            vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
            -- Code action groups
            vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
        end,

        on_init = function(client)
            local path = client.workspace_folders[1].name

            if path == "/mnt/storage/Projects/rust/new_lavalink" then
                client.config.settings["rust-analyzer"].cargo.features = {"python"}
            end

            client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })

            return true
        end,

        settings = {
            ["rust-analyzer"] = {
                diagnostics = {
                    disabled = {
                        "missing-unsafe",
                        "unresolved-proc-macro",
                        "unresolved-macro-call",
                        "macro-error"
                    },
                },
                cargo = {
                    features = {}
                }
            }
        }
    },
})

require('crates').setup {
    completion = {
        coq = {
            enabled = true,
            name = "crates.nvim",
        },
    },
}

-- get search results for trouble
local actions = require("telescope.actions")
local trouble = require("trouble.sources.telescope")

local telescope = require("telescope")

telescope.setup {
    defaults = {
        mappings = {
            i = { ["<c-t>"] = trouble.open },
            n = { ["<c-t>"] = trouble.open },
        },
    },
}

-- Treesitter Plugin Setup
require('nvim-treesitter.configs').setup {
    ensure_installed = { "lua", "rust", "toml" },
    auto_install = true,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting=false,
    },
    ident = { enable = true },
    rainbow = {
        enable = true,
        extended_mode = true,
        max_file_lines = nil,
    }
}

-- 3rd party autocomplete scripts
require("coq_3p") {
    { src = "builtin/c" },
    { src = "builtin/html" },
    { src = "builtin/js" },
    { src = "nvimlua", short_name = "nLUA", conf_only = true },
    { src = "vimtex",  short_name = "vTEX" },
    --{ src = "copilot", short_name = "COP", accept_key = "<c-f>" },
    { src = "codeium", short_name = "COD" },
}
--vim.g.codeium_render = false

-- NeoTree setup
require("neo-tree").setup({
    close_if_last_window = true,
    window = {
        width = 35,
    },
    filesystem = {
        follow_current_file = {
            enabled = true,
            leave_dirs_open = true,
        },
        filtered_items = {
            visible = true,
            show_hidden_count = true,
            hide_dotfiles = false,
            hide_gitignored = false,
        },
    },
    buffers = {
        follow_current_file = {
            enable = true,
        },
    },
})
